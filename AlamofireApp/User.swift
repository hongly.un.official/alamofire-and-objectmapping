//
//  Article.swift
//  AlamofireApp
//
//  Created by hongly on 12/13/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import ObjectMapper


class User: Mappable {
    
    var id: Int?
    var name: String?
    var email: String?
    var gender: String?
    var telephone: String?
    var status: String?
    var fbId: String?
    var imgUrl: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["ID"]
        name <- map["NAME"]
        email <- map["EMAIL"]
        gender <- map["GENDER"]
        telephone <- map["TELEPHONE"]
        status <- map["STATUS"]
        fbId <- map["FACEBOOK_ID"]
        imgUrl <- map["IMAGE_URL"]
       
    }
}
